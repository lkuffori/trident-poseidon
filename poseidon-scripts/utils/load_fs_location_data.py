import os

# Relative FS prefix of the location of the workflows data
FS_PREFIX = "../../data/"

def load_trident_data_fs_locations():
    """
    :return: (Tuple)->(object,array): Object with 'benchmarks' and 'applications' keys
    which separate the list of workflows by the two groups. Array with ALL the workflows in
    a flattened list.
    """
    benchmarks = load_trident_data_locations(FS_PREFIX + "benchmarks/", experiment_type="benchmarks")
    applications = load_trident_data_locations(FS_PREFIX + "applications/", experiment_type="applications")

    all_data_flattened = {**benchmarks, **applications}

    all_data = {
        "benchmarks": benchmarks,
        "applications": applications
    }
    return all_data, all_data_flattened


def load_trident_data_locations(WORKFLOWS_FS_PREFIX, experiment_type="experiment"):
    """
    Obtain the Trident Data files FS relative path.
    :param WORKFLOWS_FS_PREFIX: (String) FS location of workflows)
    :param experiment_type: (String) Experiment type for logs messages
    :return: (list<objects>) List of objects with the following keys:
     - "balanced_fn" (optional)
     - "processed_fn"
     - "logs_fn"
     - "raw_fn"
    Containing the FS locations of the respective Trident data files
    for all of the experiments inside WORKFLOWS_FS_PREFIX parameter.
    """
    experiments = {}
    for experiment_name in os.listdir(WORKFLOWS_FS_PREFIX):
        for file_name in os.listdir(os.path.join(WORKFLOWS_FS_PREFIX, experiment_name)):
            identifier = experiment_name + "." + ".".join(file_name.split(".")[:1])
            path_to_file = os.path.join(WORKFLOWS_FS_PREFIX, experiment_name, file_name)
            if identifier not in experiments:
                experiments[identifier] = {}
            if "balanced" in file_name:
                experiments[identifier]["balanced_fn"] = path_to_file
            elif file_name.endswith(".proc"):
                experiments[identifier]["processed_fn"] = path_to_file
            elif file_name.endswith(".log"):
                if len(file_name.split(".")) <= 3:
                    experiments[identifier]["logs_fn"] = path_to_file
                else:
                    experiments[identifier]["raw_fn"] = path_to_file
    print (len(experiments), experiment_type , " were loaded.")
    return experiments




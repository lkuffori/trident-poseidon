def prepare_mts_matrix(df, sample_number, class_label=None, training_method="WEASEL"):
    """
    Auxiliary function to help in the formatting of the training and
    evaluation matrix for the transformation + classification method.
    **Only WEASEL+MUSE implemented**
    :param df: (Pandas Dataframe) Raw dataframe
    :param training_method: (String) Specify the way of formatting the matrix.
    :param sample_number: (Integer~ONLY for WEASEL method) Index of current sample.
    :param class_label: (Integer~ONLY for WEASEL method) Class label of the current sample
    If WEASEL, this parameter should be the Benchmark identifier
    :return:
    """
    del df['Timestamp']
    del df['TIMESTAMP']
    del df['S0 C0 READ BW (MIB)']
    del df['S0 C1 READ BW (MIB)']
    if training_method == "WEASEL":
        df.insert(0, 'benchmark', [sample_number] * len(df))
        df.insert(1, 'index', range(1, len(df) + 1))
        if class_label:
            df.insert(2, 'class', [class_label] * len(df))
        else:
            df.insert(2, 'class', [sample_number] * len(df))
    return df

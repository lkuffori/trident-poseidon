# coding=utf-8
import pandas as pd
import ntpath
import numpy as np
import math
from datetime import datetime
from fastdtw import fastdtw
from scipy.spatial import distance
from pandas.api.types import is_numeric_dtype
from scipy.stats import gmean


'''
Class to read and merge trident metrics
'''

DATE_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ' # date format used
ITERATION_LINE_SEPARATOR = '->' # line separator on logs Trident Files
START_END_SEPARATOR = ';'
TRIDENT_LOG_SEPARATOR = ';' # line separator on trident files
DTW_PRECISION = 20


class TridentFileReader:

    def __init__(self, verbose=False):
        self.verbose = verbose
        self.data = None
        self.balanced_data = pd.DataFrame()
        self.marks = {
            "iterations": [],
            "ends": [],
            "starts": []
        }
        pass

    def read_file(self, file_path, file_type="csv", index="Timestamp"):
        # get file name from path
        file_name = self.get_name_from_path(file_path)
        if self.verbose: print ("TridentFileReader is Reading File...")
        df = None
        if file_name.endswith(".log"):
            if self.verbose: print ("File Type: Raw Metrics")
            df = pd.read_csv(file_path, sep=TRIDENT_LOG_SEPARATOR, error_bad_lines=False)
        elif file_name.endswith(".proc"):
            if self.verbose: print ("File Type: Processed Metrics")
            df = pd.read_csv(file_path, sep=TRIDENT_LOG_SEPARATOR, index_col=False, error_bad_lines=False)
        else:
            if self.verbose: print ("File Type: Balanced data")
            df = pd.read_csv(file_path, sep=TRIDENT_LOG_SEPARATOR, error_bad_lines=False)
        return df

    def merge_metrics(self, raw_fn, processed_fn):

        raw_df = self.read_file(raw_fn).iloc[:, :-1]  # deleting last empty column
        processed_df = self.read_file(processed_fn).iloc[:, :-1]

        # merge in just one dataframe all the metrics
        concatenated_df = pd.concat([raw_df, processed_df],
                                    axis=1)

        self.clean_column_names(concatenated_df)

        # Delete unuseful columns, ---, RM BW, TOT BW
        concatenated_df.drop(inplace=True,
                             columns=["---",
                                      " \"---\"",
                                      " \"RM BW\"",
                                      " \"TOT BW\"",
                                      "RM BW", "TOT BW"],
                             errors='ignore')
        concatenated_df.dropna(subset=["Timestamp"], inplace=True)
        concatenated_df.set_index("Timestamp", inplace=True, drop=False)
        self.data = concatenated_df
        return concatenated_df

    def get_run_marks(self, logs_fn):
        metrics_starting_timestamp = self.data["Timestamp"].iloc[0]
        metrics_initial_str_timestamp = self.data["TIMESTAMP"].iloc[0]
        metrics_initial_dt = datetime.strptime(metrics_initial_str_timestamp, DATE_FORMAT)
        for line in open(logs_fn):
            line = line.strip()
            if line.startswith("Iteration "):
                self.get_iteration_marks(line, metrics_starting_timestamp, metrics_initial_dt)
            elif line.endswith("Start;"):
                self.get_endstart_marks(line, "start", metrics_starting_timestamp, metrics_initial_dt)
            elif line.endswith("End;"):
                self.get_endstart_marks(line, "end", metrics_starting_timestamp, metrics_initial_dt)
        return self.marks

    def get_iteration_marks(self, info, metrics_starting_timestamp, metrics_initial_dt):
        temp1, temp2 = info.split(ITERATION_LINE_SEPARATOR)
        identifier, start = temp1.split(" : ")
        start = datetime.strptime(start.strip(), DATE_FORMAT)
        end = temp2.split(",")[0].strip()
        end = datetime.strptime(end, DATE_FORMAT)
        self.marks["iterations"].append({
            "identifier": identifier,
            "start": metrics_starting_timestamp - (metrics_initial_dt - start).total_seconds(),
            "end": metrics_starting_timestamp - (metrics_initial_dt - end).total_seconds()
        })

    def get_endstart_marks(self, line, mark_type, metrics_starting_timestamp, metrics_initial_dt):
        temp = line.split(START_END_SEPARATOR)
        identifier = temp[-2].strip()
        start = temp[1].strip()
        start = datetime.strptime(start, DATE_FORMAT)
        relative_timestamp = metrics_starting_timestamp - (metrics_initial_dt - start).total_seconds()
        if mark_type == "start":
            self.marks["starts"].append({
                "identifier": identifier,
                "time": relative_timestamp
            })
        else:
            self.marks["ends"].append({
                "identifier": identifier,
                "time": relative_timestamp
            })

    def balance_data(self, experiment_name):
        # Divide data into segments given by trident logs
        segments = [self.data[(self.data['Timestamp'] > x["start"]) &
                              (self.data['Timestamp'] < x["end"])]
                    for x in self.marks["iterations"]]
        if len(self.marks["iterations"]) == 1:
            if self.verbose: print ("There is just one iteration of the process.")
            if self.verbose: print ("No data to balance.")
            segments[0].to_csv("balanced_data_" + experiment_name + ".csv", sep=";")
            return segments[0]
        if self.verbose: print ("There are ", len(segments), " iterations of the process.")
        if self.verbose: print ("Starting to balance: ")
        for elem in segments:
            if self.verbose: print (len(elem))
        columns_to_balance = segments[0].columns.values
        if self.verbose: self.printProgressBar(0, len(columns_to_balance) * len(segments),
                              prefix='Progress:', suffix='Complete', length=50)
        # Balance every column of every segment
        # columns_to_balance = ["be bound s0"]
        for i, metric in enumerate(columns_to_balance):
            print ("===================== BALANCING ", metric, " ====================")
            self.compute_balance_dtw(segments, metric)  # compute dtw for each column
            if self.verbose: self.printProgressBar(i + 1,
                                  len(columns_to_balance),
                                  prefix='Progress:',
                                  suffix='Complete', length=50)
        self.balanced_data.to_csv("balanced_data_" + experiment_name + ".csv", sep=";")
        print ("\n\nBalanced Data File was generated successfully\n\n")
        return self.balanced_data

    @staticmethod
    def clean_column_names(df):
        df.columns = [x.replace("\"", "").strip()
                      for x in df.columns.values]

    @staticmethod
    def get_name_from_path(file_path):
        head, tail = ntpath.split(file_path)
        return tail or ntpath.basename(head)

    @staticmethod
    def split_metrics(trident_data):
        return trident_data[:, :61], trident_data[:, 61:]

    def compute_balance_dtw(self, segments, metric=None):
        """
        Dynamic Time Warping implementation for trident data.
        References:
        - Sakoe, Hiroaki, and Seibi Chiba. "Dynamic Programming Algorithm Optimization for Spoken Word Recognition."
        IEEE Transactions on Acoustics, Speech, and Signal Processing.
        Vol. ASSP-26, No. 1, 1978, pp. 43-49.
        :param metric: Metric to be analyzed
        :return: DTW plot
        """
        tmp_balanced_data = []
        if not is_numeric_dtype(segments[0][metric]):
            return
        for n in range(0, len(segments)-1):
            if n == 0:
                segment_to_add_1 = segments[n][metric].tolist()
            else:
                segment_to_add_1 = tmp_balanced_data
            segment_to_add_2 = segments[n+1][metric].tolist()
            d1, path = fastdtw(segment_to_add_1, segment_to_add_2,
                               radius=DTW_PRECISION, dist=distance.euclidean)
            if self.verbose: print ("Distance between paths: ", d1)
            new_balanced_values = self.get_average_values_from_path(path, segment_to_add_1, segment_to_add_2)
            tmp_balanced_data = new_balanced_values
        if self.verbose: print ("Len of balanced data", len(self.balanced_data))
        if self.verbose: print ("Len of tmp balanced data", len(tmp_balanced_data))
        self.balanced_data[metric] = tmp_balanced_data

    @staticmethod
    def get_average_values_from_path(path, s1, s2):
        balanced_values = []
        previous_pivot_value = -1
        values_to_merge = []
        for i, j in path:
            actual_pivot_value = i  # using the first array index as pivots
            values_to_merge.append(s1[i])
            values_to_merge.append(s2[j])
            pivot_index_change = actual_pivot_value != previous_pivot_value
            if pivot_index_change:
                try:
                    value = gmean(values_to_merge)
                    if math.isnan(value):
                        value = np.mean(values_to_merge)
                except Exception as e:
                    value = np.mean(values_to_merge)
                balanced_values.append(value)
                previous_pivot_value = actual_pivot_value
                values_to_merge = []
        print ("Len of path: ", len(path))
        print ("Length of new sequence: ", len(balanced_values))
        # plt.show()
        print ("Balanced values", balanced_values)
        return balanced_values

    @staticmethod
    def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
        """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
        """
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix))
        # Print New Line on Complete
        if iteration == total:
            print()
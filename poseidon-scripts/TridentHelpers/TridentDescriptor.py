import matplotlib.pyplot as plt
import TridentHelpers.TridentFileReader as TFR
from scipy.spatial import distance
from fastdtw import fastdtw
import scipy.ndimage.filters as filters
plt.style.use('ggplot')

'''
Class to define a trident descriptor object.
 
A TridentDescriptor is an abstraction we define to make exploratory 
analysis of Trident Data more modularized and easy to do.

- Obtain descriptive statistics of the Trident data.
- Plot a time series of any of the metrics present on the data.
- Plot an area chart representing the Intel Top Down Analysis (Fe Bound, Be Bound, Retiring, Bad Spec).
- Plot an area chart representing the memory bandwidth behavior (RBW, WBW).
- Balance the data in case there are many iterations of the workflow.
'''

MARKS_COLORS = {
    'starts': 'r',
    'ends': 'g',
    'iterations': 'b'
}
COLORS = ['firebrick', 'teal', 'yellowgreen']
IN_DEPTH_DETAIL_MARKS = False


class TridentDescriptor:

    def __init__(self, trident_data=None, raw_fn=None, logs_fn=None, processed_fn=None, period=None,
                 balance_data=False, balanced_data=None, segment=None, verbose=False, experiment=None):
        """
        TridentDescriptor constructor
        :param trident_data: (Pandas Dataframe) Dataframe containing Trident data to load
        :param raw_fn: (String) FS location of Trident raw metrics.
        :param logs_fn: (String) FS location of Trident logs.
        :param processed_fn: (String) FS location of Trident processed metrics.
        :param period: (Integer Tuple) (init, end) to take just one segment of the given data.
        :param balance_data: (Boolean) If True, data is balanced using DTW
        :param balanced_data: (String) FS location of Trident balanced data.
        :param segment: (Integer) Integer representing the segment to use, if possible.
        :param verbose: (Boolean) If True, all logs messages from TridentFileReader are presented.
        :param experiment: (String) Experiment name to use in plots and logs messages.
        :return: TridentDescriptor object
        """
        file_reader = TFR.TridentFileReader(verbose=verbose)
        if raw_fn and processed_fn:
            self.experiment = file_reader.get_name_from_path(raw_fn)[: 15]  # just to have a reference name to show
            if experiment:
                self.experiment = experiment
            trident_data = file_reader.merge_metrics(raw_fn, processed_fn)
        if logs_fn:
            self.logs_marks = file_reader.get_run_marks(logs_fn)
        # If there is more than 1 iteration, balance the data
        if balance_data and self.logs_marks:
            file_reader.balance_data(self.experiment)  # generate balanced data file
            print ("No balancing required/possible for: ", self.experiment)
        if balanced_data:
            self.experiment = file_reader.get_name_from_path(balanced_data)[: 15]
            trident_data = file_reader.read_file(balanced_data)
        if period and trident_data is not None: # Custom segment of the data
            init = period[0]
            end = period[1]
            trident_data = trident_data.iloc[init:end]
        self.data = trident_data
        if segment is not None:
            self.data = self.get_iterations()[segment]
        print ("Descriptor of ", experiment, " created successfully")

    def describe(self, metric=None):
        """
        Obtain descriptive statistic of entire data or one metric
        :param metric: (list or String) Metric(s) to describe (optional)
        :return: Dataframe with description
        """
        if metric:
            if type(metric) == list:
                print (self.data.describe()[metric])
                return self.data.describe()[metric]
            else:
                print (self.data.describe()[[metric]])
                return self.data.describe()[[metric]]
        else:
            print (self.data.describe())
            return self.data.describe()

    def time_series_plot(self, metric, marks=False, smooth=False, show=True, subplot=None, color="firebrick"):
        """
        Plot a Time Series of one metric of the data
        :param metric: (String) Metric to plot.
        :param marks: (bool) If True, marks are added to the plot.
        :param smooth: (bool) If True, values of metric are smoothed with a Gaussian Filter.
        :param show: (bool) If True, show the plot.
        :param subplot: (bool) If true, use the plot as a subplot
        :param color: (String) Color to use for the plot interpolation
        :return: Current axes of the current figure
        """
        values_to_plot = self.data[[metric]]
        values_to_plot.drop(values_to_plot[metric].idxmax(), inplace=True)
        values_to_plot.drop(values_to_plot[metric].idxmin(), inplace=True)
        if smooth:
            smoothed_values = filters.gaussian_filter1d(values_to_plot[metric],
                                                        sigma=2.5)
            if subplot:
                subplot.plot(smoothed_values, lw=0.50, label="Smoothed "+metric, color=color)
            else:
                plt.plot(smoothed_values, lw=0.50, label="Smoothed " + metric, color=color)
        else:
            if subplot:
                subplot.plot(values_to_plot, lw=0.20, label=metric, color=color)
            else:
                plt.plot(values_to_plot, lw=0.20, label=metric, color=color)
        ax = plt.gca()
        ax.set_xlabel(self.data.index.name)
        ax.set_ylabel(metric.capitalize())
        ax.set_title(self.experiment + ": " + metric)
        if subplot:
            subplot.set_xlabel(self.data.index.name)
            subplot.set_ylabel(metric.capitalize())
            subplot.set_title(self.experiment + ": " + metric)
        if marks and self.logs_marks:
            self.add_plot_marks(ax)
        if show:
            plt.show()
        return plt.gca()

    def get_iterations(self):
        """
        Get each of the iterations of the monitored data
        :return: List of Dataframes
        """
        return [self.data[(self.data['Timestamp'] > x["start"]) &
                          (self.data['Timestamp'] < x["end"])]
                for x in self.logs_marks["iterations"]]

    def iterations_plot(self, metric, calculate_dtw=False):
        """
        Plot a metric TS on different iterations. If calculate_dtw = True,
        the path best matches is plotted.
        :param metric: Metric to plot
        :param calculate_dtw: If True, DWT is computed for the first and second iterations of the metric data
        :return: None
        """
        segments = self.get_iterations()
        if calculate_dtw:
            d1, path = fastdtw(segments[0][metric], segments[1][metric], radius=1, dist=distance.euclidean)
            print (d1)

            x = [k for k in range(len(segments[0][metric]))]
            y = [z for z in range(len(segments[1][metric]))]
            plt.plot(x, segments[0][metric], color='teal')
            plt.plot(y, segments[1][metric], color='firebrick')
            s1 = segments[0][metric].tolist()
            s2 = segments[1][metric].tolist()
            print (len(path))
            for i, j in path:
                plt.plot((i, j), (s1[i], s2[j]), color='k', lw=0.2)
            plt.show()
        else:
            for j, elem in enumerate(segments):
                series = elem[metric]
                series.drop(series.idxmax(), inplace=True)
                series.drop(series.idxmin(), inplace=True)
                index = [i for i in range(len(series))]
                plt.plot(index, series, color=COLORS[j], lw=0.4, linestyle="--")
            # index = [p for p in range(len(df[metric]))]
            # plt.plot(index, df[metric], 'k', lw=0.8)
            plt.show()

    def top_down_plot(self, socket="s0", marks=False, show=True, subplot=None):
        """
        Plot the Intel Top Down Analysis (Fe bound, Be bound, Bad Spec & Retiring)
        :param socket: (String) Socket on which to plot the Top Down Analysis.
        :param marks: If True, marks are added to the plot.
        :param show: If true, show the plot
        :param subplot: If true, use the plot as a subplot
        :return: Current figure
        """
        parameters = ["fe bound", "bad spec", "retiring", "be bound"]
        parameters = [x + " " + socket for x in parameters]
        if not subplot:
            ax = abs(self.clean_tdp_data(self.data[parameters])) \
                .plot.area(lw=0.1, color=['blueviolet', 'indianred', 'yellowgreen',  'dodgerblue'])
        else:
            ax = abs(self.clean_tdp_data(self.data[parameters])) \
                .plot.area(ax=subplot, lw=0.1, color=['blueviolet', 'indianred', 'yellowgreen', 'dodgerblue'])
        ax.set_xlabel(self.data.index.name)
        ax.set_ylabel("Top Down Analysis")
        ax.set_title(self.experiment + ": " + "Top Down Analysis")
        if marks and self.logs_marks:
            self.add_plot_marks(ax)
        if show:
            plt.ylim((0, 1.1))
            plt.show()
        return plt.gcf()

    def memory_bw_plot(self, socket="s0", marks=False, show=True, subplot=None):
        """
        Plot a Memory Bandwidth plot (WBW, RBW)
        :param socket: (String) Socket on which to plot the Memory Bandwidth Analysis
        :param marks: If True, marks are added to the plot.
        :param show: If true, show the plot
        :param subplot: If true, use the plot as a subplot
        :return: Current figure
        """
        parameters = ["RBW", "WBW"]
        parameters = [socket.upper() + " " + x for x in parameters]
        if not subplot:
            ax = abs(self.data[parameters]).plot.area(lw=0.1,
                                                  color=['teal', 'firebrick'])
        else:
            ax = abs(self.data[parameters]).plot.area(ax=subplot, lw=0.1,
                                                  color=['teal', 'firebrick'])
        ax.set_xlabel(self.data.index.name)
        ax.set_ylabel("Memory Bandwidth (MIB)")
        ax.set_title(self.experiment + ": " + "Memory Bandwidth Analysis")
        if marks and self.logs_marks:
            self.add_plot_marks(ax)
        if show:
            plt.show()
        return plt.gcf()
        
    def add_plot_marks(self, ax):
        """
        Add the iteration marks to a plot.
        :param ax: (Axes) Plot to add the marks
        """
        y_lim = ax.get_ylim()[1]
        for mark_type, marks in self.logs_marks.items():
            for mark in marks:
                if mark_type == "iterations":
                    plt.axvline(x=mark["start"], color=MARKS_COLORS[mark_type], linestyle='--', lw=0.5)
                    plt.text(mark["start"] + 1, y_lim, mark["identifier"] + " start", rotation=90, size=8)
                    plt.axvline(x=mark["end"], color=MARKS_COLORS[mark_type], linestyle='--', lw=0.5)
                    plt.text(mark["end"] + 1, y_lim, mark["identifier"] + " end", rotation=90, size=8)
                else:
                    if IN_DEPTH_DETAIL_MARKS:
                        plt.axvline(x=mark["time"] + 1, color=MARKS_COLORS[mark_type], linestyle='--', lw=0.5)
                        plt.text(mark["time"] + 1, y_lim, mark["identifier"], rotation=90, size=8)

    @staticmethod
    def clean_tdp_data(data):
        columns = data.columns.values
        for field in columns:
            data = data[data[field] < 1]
        return data

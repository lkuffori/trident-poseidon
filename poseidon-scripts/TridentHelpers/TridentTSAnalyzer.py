import matplotlib.pyplot as plt
import scipy.ndimage.filters as filters
import math
import time
import ruptures as rpt
from fastdtw import fastdtw
from sklearn.metrics.pairwise import *
from pyts.transformation import WEASEL
from pyts.classification import BOSSVSClassifier
from pyts.image import GASF
np.set_printoptions(threshold=np.nan)
plt.style.use('ggplot')

'''
TridentTSAnalyzer is the abstraction we define to make Trident 
Time Series analysis, comparation and measurements more modularized and easy to do. 
TridentTSAnalyzer main purpose is to compare Trident Data of two different applications.

- Compute univariate WEASEL transformation and measure similarity
between the resulting Bag of Patterns histogram of the data being compared.
- Plot a time series graphic of a matric of the both analyzed 
Trident Data in the same canvas.
- Find the different segments embedded in a workflow. 
'''

WEASEL_N_BINS = 8
BOSSVS_N_BINS = 4
MTF_N_BINS = 8
COLORS = ['firebrick', 'teal', 'yellowgreen', 'b', 'g', 'm', 'k']


class TridentTSAnalyzer:
    def __init__(self, application_descriptor=None, benchmark_descriptor=None, metric=None,
                 smooth=False, app_start=0, app_end=None, bench_start=0, bench_end=None,
                 max_metric=None, min_metric=None):
        """
        Create a TridentTSAnlyzer Object
        :param application_descriptor: TridentDescriptor of application data
        :param benchmark_descriptor: TridentDescriptor of benchmark data
        :param metric: (String) Metric to do the similarities measures on
        :param smooth: (Boolean) If True, If True, values of metrics are smoothed with a Gaussian Filter.
        :param app_start: (Integer) timestamp/index where to start the application
        :param app_end: (Integer) timestamp/index where to end the application (must be > app_start)
        :param bench_start: (Integer) timestamp/index where to start the benchmark
        :param bench_end: (Integer) timestamp/index where to end the benchmark (must be > bench_start)
        :param max_metric: (Integer) Delete all the values in the metric TS higher than the given value
        :param min_metric: (Integer) Delete all the values in the metric TS smaller than the given value
        """
        self.application_data = None
        self.benchmark_data = None
        if metric and application_descriptor and benchmark_descriptor:
            self.application_data = application_descriptor.data[metric].values
            self.benchmark_data = benchmark_descriptor.data[metric].values
            if max_metric:
                self.application_data = self.application_data[self.application_data < max_metric]
                self.benchmark_data = self.benchmark_data[self.benchmark_data < max_metric]
            if min_metric:
                self.application_data = self.application_data[self.application_data > min_metric]
                self.benchmark_data = self.benchmark_data[self.benchmark_data > min_metric]
            if app_end:
                self.application_data = self.application_data[app_start:app_end]
            if bench_end:
                self.benchmark_data = self.benchmark_data[bench_start:bench_end]
            if smooth:
                self.application_data = filters.gaussian_filter(self.application_data, sigma=2.5)
                self.benchmark_data = filters.gaussian_filter(self.benchmark_data, sigma=1)

    @staticmethod
    def calculate_penalty(metric, ts):
        """
        Calculate the penalty for WBS.
        :param metric: (String) Metric to calculate the WBS penalty
        :param ts: (array-like) Data to calculate the penalty.
        :return: The penalty factor
        """
        # think this better
        if "bw" in metric.lower():
            print("Penalty factor: 7")
            return np.max(ts) * float(7)
        print("Penalty factor: 3.5")
        return np.max(ts) * float(3.5)

    def wbs_segmentation(self, experiment_name, data_to_segment=None,
                         metric="fe bound s0", plot=True, penalty=None):
        """
        Segment the given application or a given array using WBS (Wild Binary Segmentation).
        References:
        - Fryzlewicz, P. (2014). Wild binary segmentation for multiple change-point detection.
        The Annals of Statistics, 42(6), 2243-2281.
        :param experiment_name: (String) Experiment name to use in plots and logs messages.
        :param data_to_segment: (array-like) if given, segmentation can be done to this custom data,
        instead of the application data from the class.
        :param metric: (String) Metric to segment the application
        :param plot: (Boolean) If true, show the plot of the segmentation.
        :param penalty: (Number) Penalty factor for WBS (threshold)
        :return: (array-like) Segments list. Every value is the index of a change point.
        """
        if self.application_data is not None:
            data_to_segment = np.array(self.application_data)
        if data_to_segment is not None:
            data_to_segment = np.delete(data_to_segment, data_to_segment.argmax())
            data_to_segment = np.delete(data_to_segment, data_to_segment.argmin())
            data_to_segment = filters.gaussian_filter(data_to_segment, sigma=5)
            print ("Building WBS model...")
            model = rpt.Binseg(model="l1", min_size=400).fit(data_to_segment)
            print("Detecting segments with WBS...")
            if penalty is not None:
                pen = penalty
            else:
                pen = self.calculate_penalty(metric, data_to_segment)
            result = model.predict(pen=pen)
            # display
            if plot:
                print("Segments Detector is Plotting Segments...")
                fig, ax = rpt.display(data_to_segment, result, result)
                ax[0].set_title(experiment_name, fontsize=22)
                plt.show()
            return result
        else:
            print("No data to segment")

    def dtw(self, metric=None):
        """
        Dynamic Time Warping implementation for trident data between application
        data and benchmark data on the values of a metric.
        References:
        - Sakoe, Hiroaki, and Seibi Chiba. "Dynamic Programming Algorithm Optimization for Spoken Word Recognition."
        IEEE Transactions on Acoustics, Speech, and Signal Processing.
        Vol. ASSP-26, No. 1, 1978, pp. 43-49.
        :param metric: (String) Metric to be analyzed
        :return: DTW plot
        """
        if metric:
            application_values = self.application_data[metric]
            benchmark_values = self.benchmark_data[metric]
            d1, path = fastdtw(application_values, benchmark_values, radius=10, dist=distance.euclidean)
            index_a = [i for i in range(len(application_values))]
            index_b = [i for i in range(len(benchmark_values))]
            plt.plot(index_a, application_values, color='firebrick')
            plt.plot(index_b, benchmark_values, color='teal')
            print ("Time Warped Distance: ", d1)
            for i, j in path:
                plt.plot((i, j), (application_values[i], benchmark_values[j]),
                         color='b', lw=0.02)
            plt.show()

    def weasel(self, metric=None):
        """
        Apply WEASEL transformation and cosine similarity of the resulting bag of patterns
        model histogram to calculate similarity between the benchmark and application data
        on an specified metric.
        References:
        -  Schäfer, P., & Leser, U. (2017, November). Fast and accurate time series classification with weasel.
        In Proceedings of the 2017 ACM on Conference on Information and Knowledge Management (pp. 637-646). ACM.
        :param metric: (String) Metric to be analyzed
        """
        if metric:
            # Preparing the vectors
            application_values = self.application_data
            benchmark_values = self.benchmark_data
            print ("Size of Application TS: ", len(application_values))
            print ("Size of Benchmark TS: ", len(benchmark_values))
            # Selecting proper windowing value (20% of smaller array)
            window_lenght = len(application_values)*0.01 if len(application_values) > len(benchmark_values) else len(benchmark_values)*0.01
            window_lenght = int(math.floor(window_lenght))
            # Multiple windows, half and quarter of original window
            windows_sizes = [15, 21, 10]
            # Coefficients of TFT to take is defined as the smaller window
            n_coefs = 10
            p_value_chi_square = 0.1  # P_VALUE to discriminize features
            print ("WEASEL Windows Sizes: ", windows_sizes)
            print ("WEASEL N BINS (size of alphabet): ", WEASEL_N_BINS)
            print ("WEASEL N COEFS (TFT coefficients to keep)", n_coefs)
            print
            print ("=== EXECUTING WEASEL ===")
            start_time = time.time()
            print ("Building model...")
            weasel_model = WEASEL(n_coefs=n_coefs,
                                  window_sizes=windows_sizes,
                                  n_bins=WEASEL_N_BINS,
                                  pvalue_threshold=p_value_chi_square,
                                  variance_selection=True,
                                  norm_mean=False,
                                  norm_std=False)
            print ("Fitting application & benchmark TS...")
            # Fill missing data with 0 (different length array)
            raw_data = self.numpy_fillna(np.array([
                application_values,
                benchmark_values
            ]))
            # Fit transform data. Assume all TS are from different classes.
            tmp_classes = range(0, len(raw_data))
            weasel_transformed = weasel_model.fit_transform(raw_data, tmp_classes)
            print ("\nFinished")
            execution_time = (time.time() - start_time)
            print ("WEASEL VECTORS\n", weasel_transformed.toarray())
            print (len(weasel_transformed.toarray()))
            print ("BAG OF PATTERNS VOCABULARY", weasel_model.vocabulary_)
            print (len(weasel_model.vocabulary_))
            print ("\n\nTime taken:\n\n", execution_time)
            print ("COSINE SIMILARITY BETWEEN VECTORS\n", cosine_similarity(self.filter_transformed_arrays(weasel_transformed.toarray())))

    def time_series_comparison(self, metric=None, title=""):
        """
        Create a side to side time series plot of the specified metric
        on the application and benchmark values on that metric.
        :param metric: (String) Metric to be analyzed
        :param title: (String) Title of the plot
        """
        if metric:
            application_values = self.application_data
            benchmark_values = self.benchmark_data
            index_a = [i for i in range(len(application_values))]
            index_b = [i for i in range(len(benchmark_values))]
            plt.plot(index_a, application_values, color='firebrick', lw=0.20)
            plt.plot(index_b, benchmark_values, color='teal', lw=0.30)
            ax = plt.gca()
            ax.set_xlabel("Index")
            ax.set_ylabel(metric.capitalize(), fontsize=18)
            ax.set_title(title, fontsize=22)
            plt.show()

    def bossvs(self, metric=None):
        """
        **Not tested**
        BOSSVS analysis.
        References: Schäfer, P. (2015). The BOSS is concerned with time series
        classification in the presence of noise.
        Data Mining and Knowledge Discovery, 29(6), 1505-1530.
        :param metric: (String) Metric to be analyzed
        """
        if metric:
            # Preparing the vectors
            application_values = self.application_data[metric].values
            benchmark_values = self.benchmark_data[metric].values
            print ("Size of Application TS: ", len(application_values))
            print ("Size of Benchmark TS: ", len(benchmark_values))
            # Selecting proper windowing value (20% of smaller array)
            print ("BOSSVS Window Size: ", "21")
            print ("BOSSVS N BINS (size of alphabet): ", BOSSVS_N_BINS)
            print ("BOSSVS N COEFS (TFT coefficients to keep): ", "all")
            print ()
            print ("=== EXECUTING BOSSVS ===")
            start_time = time.time()
            print ("Building model...")
            bossvs_model = BOSSVSClassifier(n_coefs=None,
                                            window_size=21,
                                            n_bins=BOSSVS_N_BINS,
                                            variance_selection=True,
                                            norm_mean=False,
                                            norm_std=False,
                                            numerosity_reduction=True,
                                            smooth_idf=True,
                                            sublinear_tf=True)
            print ("Fitting application & benchmark TS...")
            # Fill missing data with 0 (different length array)
            raw_data = self.numpy_fillna(np.array([
                application_values,
                benchmark_values
            ]))
            # Fit transform data. Assume all TS are from different classes.
            tmp_classes = range(0, len(raw_data))
            bossvs_model.fit(raw_data, tmp_classes)
            boss_tfidf = bossvs_model.tfidf_.toarray()
            print ("\nFinished")
            execution_time = (time.time() - start_time)
            print (len(bossvs_model.vocabulary_))
            print ("\n\nTime taken:\n\n", execution_time)
            benchmark_pivoted_values = self.filter_transformed_arrays(boss_tfidf)
            print (len(benchmark_pivoted_values[1]))
            print ("COSINE SIMILARITY BETWEEN VECTORS\n", cosine_similarity(benchmark_pivoted_values))

    def gramian_transition_field(self, metric=None):
        """
        **Not tested**
        GTF implementation.
        References: Wang, Z., & Oates, T. (2015). Imaging time-series to improve classification and imputation.
        arXiv preprint arXiv:1506.00327.
        :param metric: (String) Metric to be analyzed
        """
        if metric:
            # Preparing the vectors
            application_values = self.application_data[metric].values
            benchmark_values = self.benchmark_data[metric].values
            print ("Size of Application TS: ", len(application_values))
            print ("Size of Benchmark TS: ", len(benchmark_values))
            image_size = 512
            print ("GTF image Sizes: ", image_size)
            print ()
            print ("=== EXECUTING GTF ===")
            start_time = time.time()
            print ("Building model...")
            mtf = GASF(image_size=image_size,
                       overlapping=False)
            print ("Fitting application & benchmark TS...")
            # Fill missing data with 0 (different length array)
            raw_data = self.numpy_fillna(np.array([
                application_values[application_values < 2],
                benchmark_values[benchmark_values < 2]
            ]))
            print (len(raw_data[0]))
            print (len(raw_data[1]))
            # Fit transform data. Assume all TS are from different classes.
            mtf_transformed = mtf.fit_transform([raw_data[0]])
            mtf_transformed2 = mtf.fit_transform([raw_data[1]])
            print ("\nFinished")
            execution_time = (time.time() - start_time)
            plt.figure(figsize=(8, 8))
            plt.imshow(mtf_transformed[0], cmap='rainbow', origin='lower')
            plt.show()
            plt.figure(figsize=(8, 8))
            plt.imshow(mtf_transformed2[0], cmap='rainbow', origin='lower')
            plt.show()
            #print "COSINE SIMILARITY BETWEEN VECTORS\n", cosine_similarity(mtf_transformed)

    @staticmethod
    def filter_transformed_arrays(A):
        # assume that benchmark is on 2nd index
        ix = np.not_equal(0, A[1])
        return np.array([
            A[0][ix],
            A[1][ix]
        ])

    @staticmethod
    def numpy_fillna(data):
        """
        Ref: https://stackoverflow.com/a/32043366/9453904
        """
        # Get lengths of each row of data
        lens = np.array([len(i) for i in data])

        # Mask of valid places in each row
        mask = np.arange(lens.max()) < lens[:, None]

        # Setup output array and put elements from data into masked positions
        out = np.zeros(mask.shape, dtype=data.dtype)
        out[mask] = np.concatenate(data)
        return out

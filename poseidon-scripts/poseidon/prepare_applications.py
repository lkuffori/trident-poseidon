import sys
import pandas as pd
from TridentHelpers.TridentDescriptor import TridentDescriptor
from utils.load_fs_location_data import load_trident_data_locations
from utils.prepare_mts_matrix import prepare_mts_matrix

"""
PREPARE APPLICATIONS DATA

Read Trident data of applications from /applications using TridentDescriptor objects and write a matrix formatted as 
previously stated under /data directory. In addition to this, it write on  /temps a file with information of the
utilized applications to maintain a register of the names of the applications mapped with the given sample 
index {names~>index}. This is done since in the formatting, the name of the application is lost. 
INPUT: ../../data/benchmarks/*
ARGUMENTS: 
    - USE_BALANCED_DATA: If True, DTW is used to balance data. Usually in applications, this doest not make a difference
    - TRAINING_METHOD: Only WEASEL, as of now
    - FS_OUTPUT: Evaluation matrix output name
EXTRA PARAMETERS:
    - CONTROLLED_EXPERIMENT: If True, the last iteration of the benchmarks is used for evaluating.
    - COLUMNS_TO_USE: List of names of columns to filter (refer to ./temps/feature_mapping.txt for names)
OUTPUT: WRITE APPLICATIONS EVALUATION MATRIX FILE
"""

arguments_length = len(sys.argv)

FS_PREFIX = "../../data/applications/"

USE_BALANCED_DATA = False
if arguments_length > 1 and str(sys.argv[1]):
    USE_BALANCED_DATA = bool(str(sys.argv[1]))

TRAINING_METHOD = "WEASEL"
if arguments_length > 2 and str(sys.argv[2]):
    TRAINING_METHOD = str(sys.argv[2])

FS_OUTPUT = "eval_data.csv"
if arguments_length > 3 and str(sys.argv[3]):
    FS_OUTPUT = str(sys.argv[3])

CONTROLLED_EXPERIMENT = False
if CONTROLLED_EXPERIMENT:
    FS_PREFIX = "../../data/benchmarks/"

FEATURES_MAPPING = pd.read_csv("./temps/features_mapping.txt", sep=";")
FEATURES_TO_USE = [
    "sample", "register", "benchmark_label",
    "fe bound s0", "bad spec s0", "retiring s0",
    "be bound s0", "fe bound s1",
    "bad spec s1", "retiring s1", "be bound s1",
    "S0 RBW", "S0 WBW", "S1 RBW", "S1 WBW"
]
COLUMNS_TO_USE = FEATURES_MAPPING[FEATURES_MAPPING['feature'].isin(FEATURES_TO_USE)]["mapped_index"].values


TEMPORARY_REG = open("./temps/applications_register.txt", "w")
TEMPORARY_REG.write("application_name;application_mapping\n")
if __name__ == "__main__":
    training_matrix = pd.DataFrame()
    experiments = load_trident_data_locations(FS_PREFIX)
    APPLICATIONS_USED = 0 # sample index
    CLASS_LABEL = 0 # class index
    for experiment_name, files in experiments.items():
        print ("Preparing ", experiment_name)
        raw_fn = files["raw_fn"]
        processed_fn = files["processed_fn"]
        logs_fn = files["logs_fn"]
        if USE_BALANCED_DATA:
            try:
                balanced_fn = files["balanced_fn"]
            except Exception as e:
                print ("Balanced metrics want to be used. "
                      "But no balanced_data file for ", experiment_name, ", is present. "
                      "Use ./processors/file_processor.py to create a balanced_data file.")
            td = TridentDescriptor(balanced_data=balanced_fn)
            APPLICATIONS_USED += 1 # update sample index
            training_matrix = training_matrix.append(
                prepare_mts_matrix(td.data, APPLICATIONS_USED, training_method=TRAINING_METHOD)  # add columns
            )
            TEMPORARY_REG.write(str(experiment_name) + ";" + str(APPLICATIONS_USED) + "\n") # write mapping on register
        else:
            td = TridentDescriptor(raw_fn=raw_fn, processed_fn=processed_fn,
                                   logs_fn=logs_fn, balance_data=True)
            segments = td.get_iterations()
            if CONTROLLED_EXPERIMENT:
                if len(segments) <= 1:
                    continue
                segments = segments[-1:]  # if controlled experiment, use only the last iteration
            CLASS_LABEL += 1
            for segment in segments:
                APPLICATIONS_USED += 1 # update sample index
                training_matrix = training_matrix.append(
                    prepare_mts_matrix(segment, APPLICATIONS_USED, training_method=TRAINING_METHOD, # add columns
                                      class_label=CLASS_LABEL)
                )
                TEMPORARY_REG.write(str(experiment_name) + ";" + str(APPLICATIONS_USED) + "\n") # write mapping on reg.
    if TRAINING_METHOD == "WEASEL":
        print ("Writing evaluation matrix for WEASEL classification")
        if COLUMNS_TO_USE is not None and len(COLUMNS_TO_USE) > 1: # filter columns if given
            training_matrix = training_matrix.iloc[:, COLUMNS_TO_USE]
        # write evaluation matrix
        training_matrix.to_csv("./data/" + FS_OUTPUT, sep=" ",
                               index=False, index_label=False, header=False)

        temporary_columns = open("./temps/matrix_features_mapping.txt", "w")
        # write the mapping {Metric~>ColumnNo} for every column
        temporary_columns.write("feature;mapped_index\n")
        for col_i, col in enumerate(FEATURES_TO_USE):
            temporary_columns.write(str(col) + ";" + str(col_i) + "\n")
        temporary_columns.close()

        TEMPORARY_REG.close()
import pandas as pd
import time
from poseidon_plotter import detailed_plots
from utils.load_fs_location_data import load_trident_data_locations

"""
KNOWLEDGE GENERATION

This script read the applications information, the benchmark information and the prediction results under /temps, 
and if available, the knowledge database under /data. Finally, it do a mapping between all of these components to 
produce knowledge. 
In addition to this, using the help of poseidon_plotter.py script, it can plot the results on a canvas.

INPUT: 
    - ./data/eval_data.csv 
    - ./data/train_data.csv
    - ./temps/applications_register.txt
    - ./temps/benchmarks_register.txt
    - ./temps/weasel_predictions.txt
EXTRA PARAMETERS:
    - PLOT: If true, results are plotted.
    - FS_BENCHMARK_PREFIX: FS location of benchmarks
    - FS_APPLICATIONS_PREFIX: FS location of applications
OUTPUT: POSEIDON REPORT (& PLOTS in ./results)
"""

PLOT = True
FS_BENCHMARK_PREFIX = "../../data/benchmarks/"
FS_APPLICATIONS_PREFIX = "../../data/applications/"


def generate_report(information):
    benchmark_files = load_trident_data_locations(FS_BENCHMARK_PREFIX)
    applications_files = load_trident_data_locations(FS_APPLICATIONS_PREFIX)
    applications_number = max(information["application_mapping"].values)
    print ("POSEIDON has analyzed", applications_number, "applications")
    for i in range(1, applications_number + 1): # iterate over the applications
        application_info = information.loc[information["application_mapping"] == i]
        application_segments_lenght = len(application_info)
        application_lenght = application_info["segment_end"].values[-1]
        application_name = application_info["application_name"].values[0]
        # logs messages, could be written to a file
        print ("TRIDENT monitored ", application_lenght, " timestamps of data.")
        print ("\n\nApplication No.", i, " | ", application_name, "\n")
        print ("POSEIDON detected ", application_segments_lenght,
            " different segments in the application workflow.")
        for j in range(0, application_segments_lenght): # iterate the segments of each application
            segment = application_info.iloc[j, :]
            segment_length = segment["segment_end"] - segment["segment_start"]
            segment_percentage = (float(segment_length) / application_lenght) * 100
            segment_knowledge = segment["knowledge"]
            segment_advice = segment["advice"]
            segment_match = segment["benchmark_name"]
            segment_probability = segment["probability"]
            print ("\n-->", j + 1, "th segment:")
            print ("Length: ", segment_length, " timestamps | ",
                segment_percentage,  "% of the total application.")
            print ("POSEIDON computed this segment is similar to ",
                segment_match, " with a ", str(segment_probability), "% of confidence.")
            print ("This segment can be described as follows: ", "\n".join(segment_knowledge.split("|")), "\n")
            if segment_advice:
                print ("POSEIDON advice the user to: ", "\n".join(segment_advice.split("|")), "\n")
            # plot the results nicely (refer to poseidon_plotter.py)
            if PLOT:
                texts = [

                    "\n\nApplication No." + str(i) + "  |  " + application_name + "\n",

                    "POSEIDON computed this application segment is similar to " +
                    segment_match + ", with a " + str(segment_probability*100)[:5] + "% of probability.",

                    "POSEIDON detected " + str(application_segments_lenght) +
                    " different segments in the application workflow.",

                    str(j+1) + "th segment out of " + str(application_segments_lenght) +
                    "  |  Length: " + str(segment_length) + " timestamps {" +
                    str(segment["segment_start"]) + " - " + str(segment["segment_end"]) + "}  |  " +
                    str(segment_percentage)[:5] + "% of the total application.  "

                ]
                print ("Plotting...")
                detailed_plots(segment,
                               applications_files[application_name],
                               benchmark_files[segment_match],
                               texts,
                               application_name)
            time.sleep(5)
        input("Enter to continue with next application...")


if __name__ == "__main__":
    # merge all the information together
    benchmark_register = pd.read_csv("./temps/benchmarks_register.txt", sep=";")
    applications_register = pd.read_csv("./temps/applications_register.txt", sep=";")
    weasel_predictions = pd.read_csv("./temps/weasel_predictions.txt", sep=";")
    knowledge = pd.read_csv("./data/knowledge.txt", sep=";")
    applications_register['sample_mapping'] = weasel_predictions['prediction']
    applications_register['probability'] = weasel_predictions['probability']
    information = applications_register.merge(benchmark_register,
                                             on="sample_mapping",
                                             how="left")
    information = information.merge(knowledge,
                                    on="benchmark_name",
                                    how="left")
    # create the report
    generate_report(information)


from TridentHelpers.TridentDescriptor import TridentDescriptor
from utils.load_fs_location_data import load_trident_data_fs_locations

'''
If you wish to filter metrics for classification, you may run map_features.py which will write under /temps 
a file with the {metricName~>columnNo.} mapping.  This will allow prepare_benchmarks.py and prepare_applications.py 
to use the global variable COLUMNS_TO_USE to filter the columns you wish by name.
'''

if __name__ == "__main__":
    all_data, all_data_flattened = load_trident_data_fs_locations()
    experiment, file_locations = list(all_data_flattened.items())[0]
    raw_fn = file_locations["raw_fn"]
    processed_fn = file_locations["processed_fn"]
    logs_fn = file_locations["logs_fn"]

    td = TridentDescriptor(raw_fn=raw_fn, processed_fn=processed_fn, logs_fn=logs_fn,
                           experiment=experiment)

    del td.data['Timestamp'] # timestamps not taken into account in model
    del td.data['TIMESTAMP']
    del td.data['S0 C0 READ BW (MIB)'] # this metric is corrupted
    del td.data['S0 C1 READ BW (MIB)'] # this metric is corrupted

    temporary_columns = open("./temps/features_mapping.txt", "w")
    temporary_columns.write("feature;mapped_index\n")
    temporary_columns.write("sample;0" + "\n") # first 3 columns are: sample | register | label
    temporary_columns.write("register;1" + "\n")
    temporary_columns.write("benchmark_label;2" + "\n")

    # write the mapping {Metric~>ColumnNo} for every column
    for col_i, col in enumerate(td.data.columns.values):
        temporary_columns.write(str(col) + ";" + str(col_i+3) + "\n")
    temporary_columns.close()

    print ("Mapping of features correctly done")
from subprocess import call

# Step 1 & 2: Preparing Data
call(["python", "map_features.py"])
call(["python", "prepare_benchmarks.py"])
call(["python", "prepare_applications.py"])

# Step 3: Segmenting Applications
call(["python", "segment_applications.py"])

'''
Step 4: Train & Test Model
Original Code: 
https://github.com/patrickzib/SFA
'''
call(["javac", "-cp", "lib/\\*", "-d", "classes", "@sources.txt"], cwd="../SFA")
call(["java", "-cp", "lib/\\*;./classes", "sfa.MTSClassification"], cwd="../SFA")

# Step 5: Knowledge Generation
call(["python", "knowledge_mapper.py"])
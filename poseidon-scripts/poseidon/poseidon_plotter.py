import matplotlib.pyplot as plt
import datetime
from TridentHelpers.TridentDescriptor import TridentDescriptor
from pylab import rcParams
rcParams['figure.figsize'] = 20, 25
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Tahoma', 'DejaVu Sans',
                               'Lucida Grande', 'Verdana']


def detailed_plots(segment, application_files, benchmark_files, texts, application_name):
    raw_b_fn = benchmark_files["raw_fn"]
    processed_b_fn = benchmark_files["processed_fn"]
    logs_b_fn = benchmark_files["logs_fn"]
    raw_a_fn = application_files["raw_fn"]
    processed_a_fn = application_files["processed_fn"]
    logs_a_fn = application_files["logs_fn"]
    # load entire descriptor of analyzed benchmark and application
    benchmark_d = TridentDescriptor(
        raw_fn=raw_b_fn, processed_fn=processed_b_fn,
        logs_fn=logs_b_fn, balance_data=False,
        segment=0
    )
    application_d = TridentDescriptor(
        raw_fn=raw_a_fn, processed_fn=processed_a_fn,
        logs_fn=logs_a_fn, balance_data=True,
        period=(segment["segment_start"], segment["segment_end"])
    )

    # do the plotting in grid: 4x2
    fig, axes = plt.subplots(nrows=4, ncols=2)
    plt.subplots_adjust(
        top=0.85,
        wspace=0.3,
        hspace=0.3
    )

    # top down plot & memory bw plot of each socket in benchmark and application
    application_d.top_down_plot(socket="s0", show=False, subplot=axes[0, 0])
    application_d.top_down_plot(socket="s1", show=False, subplot=axes[1, 0])
    application_d.memory_bw_plot(socket="s0", show=False, subplot=axes[2, 0])
    application_d.memory_bw_plot(socket="s1", show=False, subplot=axes[3, 0])
    benchmark_d.top_down_plot(socket="s0", show=False, subplot=axes[0, 1])
    benchmark_d.top_down_plot(socket="s1", show=False, subplot=axes[1, 1])
    benchmark_d.memory_bw_plot(socket="s0", show=False, subplot=axes[2, 1])
    benchmark_d.memory_bw_plot(socket="s1", show=False, subplot=axes[3, 1])

    # individual metrics
    '''
    application_d.time_series_plot(metric="fe bound s0", show=False, subplot=axes[0, 0], smooth=True)
    application_d.time_series_plot(metric="bad spec s0", show=False, subplot=axes[1, 0], smooth=True)
    application_d.time_series_plot(metric="retiring s0", show=False, subplot=axes[2, 0], smooth=True)
    application_d.time_series_plot(metric="be bound s0", show=False, subplot=axes[3, 0], smooth=True)
    benchmark_d.time_series_plot(metric="fe bound s0", show=False, subplot=axes[0, 1], smooth=True, color="teal")
    benchmark_d.time_series_plot(metric="bad spec s0", show=False, subplot=axes[1, 1], smooth=True, color="teal")
    benchmark_d.time_series_plot(metric="retiring s0", show=False, subplot=axes[2, 1], smooth=True, color="teal")
    benchmark_d.time_series_plot(metric="be bound s0", show=False, subplot=axes[3, 1], smooth=True, color="teal")

    '''

    # plot title and texts
    plot_title = "POSEIDON - " + datetime.datetime.now().strftime("%H:%M:%S | %B %d, %Y")
    plt.suptitle(plot_title, fontsize=24)
    plt.text(0, 1.50, texts[0], fontsize=20, ha='left', transform=axes[0,0].transAxes)
    plt.text(0, 1.45, texts[-1], fontsize=18, ha='left', transform=axes[0,0].transAxes)
    plt.text(0, 1.35, texts[1], fontsize=18, ha='left', transform=axes[0,0].transAxes)
    plt.text(0, 1.10, "APPLICATION", fontsize=24, ha='center', transform=axes[0, 0].transAxes)
    plt.text(0, 1.10, "BENCHMARK", fontsize=24, ha='center', transform=axes[0, 1].transAxes)

    plt.savefig("./results/" + application_name + "_" + str(segment["segment_start"]) + ".png", dpi=100)


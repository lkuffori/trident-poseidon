import pandas as pd
import sys
from TridentHelpers.TridentTSAnalyzer import TridentTSAnalyzer

"""
SEGMENT APPLICATION DATA

Segment the application evaluation matrix. Much of the code is for correctly formatting the matrix after doing the
segmentation with a TridentTSAnalyzer object. This script read applications matrix previously written under /data 
and it also reads the benchmarks information file under /temps. Afterwards, it proceeds to segment the application 
using WBS and re-write the application matrix correctly formatted under /data directory. In addition to this it rewrite
the application information (mapping) file under /temps.

INPUT: ./data/eval_data.csv
ARGUMENTS: 
    - FS_OUTPUT: Training matrix output name
    - PLOT: If true, plot the results of the segmentation
EXTRA PARAMETERS:
    - FS_PREFIX: Prefix FS location of the evaluation matrix
    - EVAL_FILE_NAME: File name of evaluation matrix (to be concatenated with FS_PREFIX)
    - METRIC_TO_SEGMENT: Name of the metric to segment
OUTPUT: WRITE |SEGMENTED| APPLICATION EVALUATION MATRIX FILE
"""

arguments_length = len(sys.argv)

FS_PREFIX = "./data/"

EVAL_FILE_NAME = "eval_data.csv"

FS_OUTPUT = "eval_data.csv"
if arguments_length > 1 and str(sys.argv[1]):
    FS_OUTPUT = str(sys.argv[1])

PLOT = True
if arguments_length > 2 and str(sys.argv[2]):
    PLOT = bool(str(sys.argv[2]))

METRIC_TO_SEGMENT = ["fe bound s0"]
FEATURES_MAPPING = pd.read_csv("./temps/matrix_features_mapping.txt", sep=";")
COLUMN_TO_USE = FEATURES_MAPPING[FEATURES_MAPPING['feature'].isin(METRIC_TO_SEGMENT)]["mapped_index"].values[0]


def segments_calculator(df):
    segments = {}
    # random segments for controlled experiment
    # SEGMENTS = {1: [0, 1144, 2288, 4576], 2: [0, 495, 990, 1980], 3: [0, 1384, 2768, 4154],
    #             4: [0, 1568, 3136, 4704, 6275], 5: [0, 64, 128, 192, 256, 323], 6: [0, 946, 1892, 2838, 4730],
    #             7: [0, 1018, 2036, 3054, 4072, 5093], 8: [0, 1847, 3694, 5541, 7391], 9: [0, 1369, 2738, 4107, 5478],
    #             10: [0, 2756, 5512, 8269], 11: [0, 1507, 3014, 4523], 12: [0, 1207, 3621],
    #             13: [0, 1443, 2886, 4329, 5775], 14: [0, 1420, 2840, 4262], 15: [0, 1747, 3494, 6988],
    #             16: [0, 1351, 4053]}
    applications_index_col = df[0]
    total_applications = applications_index_col.max()
    for j in range(1, total_applications + 1): # iterate over each application
        application_lenght = len(df.loc[df[0] == j])
        ts = df.loc[df[0] == j, [COLUMN_TO_USE]][COLUMN_TO_USE].values  # compute segmentation on selected metric
        print("Segments Detector is executing...")
        print ("Analyzing: ", len(ts), " points")
        app_segments = TridentTSAnalyzer().wbs_segmentation("Segment detector result",
                                                       ts, plot=False)
        app_segments.append(0)  # Segment array needs to have first and last index of TS
        app_segments = sorted(app_segments)
        app_segments.pop(-1)
        app_segments.append(application_lenght)
        print ("WBS found ", len(app_segments))
        segments[j] = app_segments
    return segments


def segment_application_matrix(df, segments):
    experiments = list(pd.read_csv("./temps/applications_register.txt", sep=";")["application_name"].values)
    TEMPORARY_REG = open("./temps/applications_register.txt", "w")
    TEMPORARY_REG.write("application_name;application_mapping;sample_no;segment_start;segment_end\n")
    phase_out = 0 # time series index gap
    application_phase = 0 # sample index gap
    total_applications = df[0].max()
    for i in range(1, total_applications + 1):
        print (i)
        application_segments = segments[i]
        for j in range(1, len(application_segments)):
            segment_start = application_segments[j-1] + phase_out
            segment_end = application_segments[j] + phase_out
            segment_lenght = segment_end - segment_start
            print ("SEGMENT START", segment_start)
            print ("SEGMENT END", segment_end)
            # sample column
            df.iloc[segment_start:segment_end, [0]] = [j+application_phase]*segment_lenght
            # register column
            df.iloc[segment_start:segment_end, [1]] = range(1, segment_lenght + 1)
            # write the mapping + segments information on the register
            TEMPORARY_REG.write(str(experiments[i - 1]) + ";" + str(i) + ";" +
                                str(j+application_phase) + ";" + str(segment_start - phase_out) + ";"
                                + str(segment_end - phase_out) + "\n")
        # entire time series gap index augment according to the segmenting index
        phase_out += application_segments[-1]
        # application index augment according to the produced segments
        application_phase += len(application_segments) - 1
    TEMPORARY_REG.close()
    # re-write the application matrix
    df.to_csv("./data/" + FS_OUTPUT, sep=" ", index=False, index_label=False, header=False)


if __name__ == "__main__":
    print ("\n\n|=============---> SEGMENTS DETECTOR <---=============|")
    # read the previously written matrix
    application_matrix = pd.read_csv(FS_PREFIX + EVAL_FILE_NAME, sep=" ", header=None)
    # obtain the list of list of segments per application
    segments = segments_calculator(application_matrix)
    # re-write the application matrix
    segment_application_matrix(application_matrix, segments)

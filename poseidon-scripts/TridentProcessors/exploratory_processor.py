from TridentHelpers.TridentDescriptor import TridentDescriptor
from utils.load_fs_location_data import load_trident_data_fs_locations


'''
Script to read, balance, plot and get insights on trident data.  
'''


# load FS location of data in list
all_data, all_data_flattened = load_trident_data_fs_locations()

if __name__ == "__main__":
    for experiment, file_locations in list(all_data_flattened.items())[2:]:
        raw_fn = file_locations["raw_fn"]
        processed_fn = file_locations["processed_fn"]
        logs_fn = file_locations["logs_fn"]
        # Creates a Descriptor for the data. If balance_data=True, DTW is used to get the
        # most representative values of data from the different iterations (logs_fn must be given)
        td = TridentDescriptor(raw_fn=raw_fn, processed_fn=processed_fn,
                               logs_fn=logs_fn, balance_data=False, verbose=False,
                               experiment=experiment)

        td.top_down_plot(marks=True)  # Get top down plot for experiment
        # td.describe('be bound s0')  # Descriptive statistics for a metric
        # td.time_series_plot('retiring s0', marks=True)  # Time series plot for a metric
        # td.iterations_plot('be bound s0', calculate_dtw=False)  # Side by side time series of different iterations
        # td.memory_bw_plot(marks=True) # Memory bandwidth plot


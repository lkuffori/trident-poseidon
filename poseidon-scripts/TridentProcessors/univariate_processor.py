from TridentHelpers.TridentTSAnalyzer import TridentTSAnalyzer
from TridentHelpers.TridentDescriptor import TridentDescriptor
from utils.load_fs_location_data import load_trident_data_fs_locations

SELECTED_BENCHMARK = 0
SELECTED_APPLICATION = 1

# load FS location of data in list
all_data, all_data_flattened = load_trident_data_fs_locations()

if __name__ == "__main__":
    benchmarks = list(all_data['benchmarks'].items())
    applications = list(all_data['applications'].items())
    PLOT_TITLE = applications[SELECTED_APPLICATION][0] + " vs. " +  benchmarks[SELECTED_BENCHMARK][0]

    # Creates a Descriptor for the data we want to compare. In this case, we create it
    # using balanced data files.
    app_td = TridentDescriptor(balanced_data=applications[SELECTED_APPLICATION][1]["balanced_fn"])
    bench_td = TridentDescriptor(balanced_data=benchmarks[SELECTED_BENCHMARK][1]["balanced_fn"])

    # List of metrics to analyze, just to have some values to use...
    metrics = "fe bound s0;bad spec s0;retiring s0;be bound s0;fe bound s1;bad spec s1;retiring s1;be bound s1;S0 RBW;" \
              "S0 WBW;S1 RBW;S1 WBW;S0 CY;S0 IN;S1 CY;S1 IN;S0 IPC;S1 IPC"
    metrics = metrics.split(";")
    metric = metrics[0]
    print()
    print (metric)
    # Instantiate the TridentTSAnalyzer object. Giving the metric to analyze.
    # If smooth=True, a Gaussian Filter is applied to smooth both signals.
    TSA = TridentTSAnalyzer(app_td, bench_td, metric, smooth=True)
    TSA.wbs_segmentation("Application Segments Detector", metric=metric, plot=True) # Find segments on the data
    TSA.time_series_comparison(metric, PLOT_TITLE + "(" + metric + ")") # Plot a TS graphic of a metric of the both analyzed descriptors in the same canvas
    if input("\nCompute WEASEL similarity? (y/n)   ") == "y":
        TSA.weasel(metric) # Compute WEASEL transformation and give a similarity measure

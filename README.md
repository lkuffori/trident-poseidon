# Trident Poseidon Project

- **Supervisors**: Servesh Muralidharan, David Smith.
- **Student**: Leonardo Kuffo (Openlab Summer Student 2018).
- **Project Report**: https://docs.google.com/document/d/1Cq9l3ffsg7Nj6R1Qc6bGn4FcdIdIsdgsi8clZz2r9LY
- **Project Activities Report**: https://docs.google.com/document/d/1VgfBxB82IjWcZ9I6kn0jBdWfP82tjIpb5ccVQclrIvE
- **Project Experiments Report**: https://docs.google.com/document/d/1qnOupMrX-tA6LbhHNuIVYnF8vUUWDd-nnIYru7VU4n8

## Overview

Improving the **performance** of an application is an important objective carried out from the application conception until its deprecation. Developers are constantly trying to improve the performance of their applications, either by using more computationally efficient algorithms, migrating to distributed processing platforms or doing intensive computation tasks on modern GPU’s. This is no surprise, given that achieving even a slight improvement in the performance of an application can be translated into the saving of time and economical resources. Every component of a system is responsible for the _running time_ of an application, which is the most tangible representation of performance. Therefore, monitoring the activity of these components on application runtime is essential in order to understand and optimize the application. **Trident** is a profiling tool which does the latter by monitoring the relevant hardware and software counters throughout the execution of an application at the node level, without inducing significant overhead. However, data produced by Trident often requires a deep understanding of computer systems at a hardware and software level to be correctly interpreted.  

The present study introduces our framework **Poseidon** which aims to _profile and describe_ a running application based entirely upon _specialized benchmarks_, turning trident data into _knowledge_ for developers. Poseidon use a novel approach of Multi-variate time series classification denominated **WEASEL + MUSE** (Word ExtrAction for time SEries cLassification + Multivariate Unsupervised Symbols and dErivatives), to measure the degree of similarity between the Trident monitored metrics of these benchmarks and the running applications. Applying our framework, we were able to profile our ground-truth data with a 96.36% of accuracy. Moreover, we _satisfactorily_ discovered different workflow phases embedded in real applications and profiled their Trident data based upon the knowledge present on benchmarks.

### Installation

To test and use the code under this repository, you need any distribution of [python 3](https://www.python.org/downloads/). Preferable, python 3.7. In addition to this, you will also need Java 8 distribution:  [Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-ubuntu-16-04) | [Centos or Fedora](https://www.digitalocean.com/community/tutorials/how-to-install-java-on-centos-and-fedora)

Next, clone this repository and install the needed dependencies using the following command:
```console
foo@bar:~/trident-poseidon/poseidon-scripts/$ pip install -r requirements.txt
```

Java dependencies are already included in the repository for simplicity.

## Trident Data Processing

Usually, Trident tool information is scattered in three files:

- Logs File: (.log) Contains information regarding the start and end of every iteration of the workload processing.
- Raw Metrics File: (.log) Contains the raw metrics monitored by Trident.
- Processed Metrics File: (.log.proc) Contains the processed Trident metrics.

See the [glossary](#metrics-glossary) for information regarding the metrics being collected by Trident.

In order to merge, balance, read and do exploratory analysis on Trident data, we defined the abstraction (i.e. class) **TridentDescriptor**. To do univariate measurements, metrics segmentation, and comparisons between Trident data, we defined the abstraction **TridentTSAnalyzer**.

### 1. TridentDescriptor ```poseidon-scripts/TridentHelpers/TridentDescriptor.py```

TridentDescriptor is the abstraction we define to make exploratory analysis of Trident Data more modularized and easy to do.

A TridentDescriptor object is able to:
- Obtain [descriptive statistics](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.describe.html) of the Trident data.
- Plot a time series of any of the metrics present on the data.
- Plot an area chart representing the Intel Top Down Analysis (Fe Bound, Be Bound, Retiring, Bad Spec).
- Plot an area chart representing the memory bandwidth behavior (RBW, WBW).
- Balance the data in case there are many iterations of the workflow (Using DTW as implemented in [fastdtw](https://pypi.org/project/fastdtw/) python package).

There are three ways to create a TridentDescriptor:
- Send the location of the three files previously mentioned (logs, raw metrics & processed metrics).
- Trident Data as a Pandas Dataframe object.
- Balanced Metrics file.

#### Usage Example ```poseidon-scripts/TridentProcessors/exploratory_processor.py```
```python
from TridentHelpers.TridentDescriptor import TridentDescriptor
file_locations = {
  "raw_fn": "...",
  "processed_fn": "...",
  "logs_fn": "..."
}
# Creates a Descriptor for the data. If balance_data=True, DTW is used to get the
# most representative values of data from the different iterations (logs_fn must be given)
td = TridentDescriptor(raw_fn=file_locations["raw_fn"], processed_fn=file_locations["processed_fn"],
                       logs_fn=file_locations["logs_fn"], balance_data=False)

td.top_down_plot(marks=True)  # Top down Analysis plot, including iteration marks on the plot
td.describe('be bound s0')  # Descriptive statistics for a metric
# Time series plot for a metric without including iteration marks with the posibility to define the plot color
td.time_series_plot('retiring s0', marks=False, color='')  
td.memory_bw_plot(marks=False) # Memory bandwidth behaviour plot plot, without iteration marks
```

If ```balance_data=True```, the TridentDescriptor proceeds to create a file with the balanced metrics using Fast Dynamic Time Warping to find the most representative value of each Time Series. The next time, you can create the descriptor  passing the parameter ```balanced_data="..."``` to the constructor. This parameter should contain the location in the File System of the created file with the balanced data.

Please, refer to the code implementation for the detailed documentation of the TridentDescriptor object.

### 2. TridentTSAnalyzer ```poseidon-scripts/TridentHelpers/TridentTSAnalyzer.py```

TridentTSAnalyzer is the abstraction we define to make Trident Time Series analysis, comparation and measurements more modularized and easy to do. TridentTSAnalyzer main purpose is to compare Trident Data of two different applications.

A TridentTSAnalyzer object is able to:
- Compute univariate WEASEL transformation and measure similarity between the resulting Bag of Patterns histogram of the data being compared (WEASEL as implemented in [pyts](https://github.com/johannfaouzi/pyts/) python package).
- Plot a time series graphic of a matric of the both analyzed Trident Data in the same canvas.
- Find the different segments embedded in a workflow. (Using Wild Binary Segmentation as implemented in [ruptures](https://github.com/deepcharles/ruptures) python package).

To create a TridentTSAnalyzer object the constructor expect two TridentDescriptor objects. From both of the data you wish to compare.

#### Usage Example ```poseidon-scripts/TridentProcessors/univariate_processor.py```
```python
from TridentHelpers.TridentTSAnalyzer import TridentTSAnalyzer
from TridentHelpers.TridentDescriptor import TridentDescriptor

metric = "fe bound s0"

# Creates a Descriptor for the data we want to compare. In this case, we create it using balanced data files.
# You can create the descriptor in the way you prefer.
app_td = TridentDescriptor(balanced_data="...")
bench_td = TridentDescriptor(balanced_data="...")

# Instantiate the TridentTSAnalyzer object. Given the metric to analyze.
# If smooth=True, a Gaussian Filter is applied to smooth both signals.
TSA = TridentTSAnalyzer(app_td, bench_td, metric, smooth=True)

TSA.wbs_segmentation("Application Segments Detector", metric=metric, plot=True) # Find segments on the application data and plot the results
TSA.time_series_comparison(metric) # Plot a TS graphic of a metric of the both analyzed descriptors in the same canvas
TSA.weasel(metric=metric) # Compute WEASEL transformation and returns a similarity measure
```

## Trident Data on File System

In our case, trident data is located under ```data/``` directory, and we have defined the following structure to store it:

.  
+-- poseidon-scripts  
+-- data  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+-- applications  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+--- ATLAS_1JUL  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +--- cmd2.log  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +--- cmd2.pmpe16.cern.ch.Trident.Beta-v3.2018-07-01T22_54_29.950782Z.log  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +--- cmd2.pmpe16.cern.ch.Trident.Beta-v3.2018-07-01T22_54_29.950782Z.log.proc  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +--- cmd1.log  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +--- ...  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+--- ...  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+-- benchmarks  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+--- HS06_lnd_28Jun  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +--- 444.namd.log  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +--- 444.namd.balanced_data  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +--- 444.namd.pmpe16.cern.ch.Trident.Beta-v3.2018-06-27T17_29_34.951130Z.log  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +--- 444.namd.pmpe16.cern.ch.Trident.Beta-v3.2018-06-27T17_29_34.951130Z.log.proc  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +--- 447.dealII.log  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +--- ...  
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+--- ...  
+-- README.md  
+-- .gitignore  

On the previous code usage examples the locations of Trident data files were entered manually. However, we have developed a method that fetch the trident data from the file system and automatically detects the four main files of trident data (logs, raw metrics, processed metrics & balanced metrics[optional]). This method is located in ``` poseidon-scripts/utils/load_fs_location_data.py ```. By entering the File System location of all the data as a global variable, the method ``` load_trident_data_fs_locations() ``` automatically loads a structure with all the detected applications/benchmarks.

#### Usage Example
```python
from TridentHelpers.TridentDescriptor import TridentDescriptor
from utils.load_fs_location_data import load_trident_data_fs_locations # import the method

# all_data: contains the data separated by benchmarks and applications
# all_data_flattened: contains the data without differentiating benchmarks and applications
all_data, all_data_flattened = load_trident_data_fs_locations() # receive the information

if __name__ == "__main__":
    for experiment_name, file_locations in list(all_data_flattened.items()): # iterate over all the experiments
        raw_fn = file_locations["raw_fn"]
        processed_fn = file_locations["processed_fn"]
        logs_fn = file_locations["logs_fn"]

        td = TridentDescriptor(raw_fn=raw_fn, processed_fn=processed_fn,
                               logs_fn=logs_fn, experiment=experiment_name, balance_data=False)

        ...
```

## Poseidon ```poseidon-scripts/poseidon/*```

Poseidon goal is to transform Trident data into knowledge. Poseidon methodology is entirely built upon benchmarks. We try to measure at which extent these benchmarks share similarity with applications, to then proceed on profiling them based on the knowledge present in the benchmarks. Poseidon pipeline is under the ```poseidon-scripts/poseidon``` directory, and it goes as follows:

![Poseidon Pipeline](https://i.imgur.com/aupTWs2.png "Poseidon Pipeline")

#### 1. & 2. Preparing Data
`MTSClassification.java` (step 4) must read data in the following format (without header):  

**Sample** | **Register** | **Label (Class)** | **Dim 1** | ... | **Dim N**
--- | --- | --- | --- | --- | ---
1 | 1 | 1 | Value X | ... | Value Y
1 | 2 | 1 | Value X | ... | Value Y
1 | 3 | 1 | Value X | ... | Value Y
2 | 1 | 1 | Value X | ... | Value Y
2 | 2 | 1 | Value X | ... | Value Y
3 | 1 | 2 | Value X | ... | Value Y
3 | 2 | 2 | Value X | ... | Value Y


**Sample** : Encodes an index for each sample in the dataset.   
**Register**: Encodes an index for each register (row || timestamp) of each sample.   
**Label**: Encodes the corresponding class (i.e. benchmark in our case) of the sample.   
**Dim**: Contains the corresponding value of each dimension of the sample in that timestamp (register).   

In order to do that, `prepare_benchmarks.py` read Trident data of benchmarks from `/benchmarks` using TridentDescriptor objects and write a matrix formatted as previously stated under `/data` directory. In addition to this, it write on   `/temps` a file with information of the utilized benchmarks to maintain a register of the names of the benchmarks mapped with the given sample index. _This is done since in the formatting, the name of the benchmark is lost_. Similarly, `prepare_applications.py` read Trident data of applications from `/applications`, write the formatted matrix under `/data` and finally it write on `/temps` a file with information of the applications (i.e. the mapping of the _{names~>index}_).  

If you wish to filter metrics for classification, you may run `map_features.py` which will write under `/temps` a file with the _{metricName~>columnNo.}_ mapping. This will allow `prepare_benchmarks.py` and `prepare_applicatinos.py` to use the global variable `COLUMNS_TO_USE` to filter the columns you wish by name.

#### 3. Segmenting Application
Segmenting applications is done in `segment_applications.py`. Much of the code is for correctly formatting the matrix after doing the segmentation with a TridentTSAnalyzer object. This script read applications matrix previously written under `/data` and it also reads the benchmarks information file under `/temps`. Afterwards, it proceeds to segment the application using WBS and re-write the application matrix correctly formatted under `/data` directory. In addition to this it rewrite the application information file under `/temps`

#### 4. Train / Test Model (Java 8)
WEASEL + MUSE transformation and logistic regression model computed with the obtained features is done in `MTSClassification.java` as implemented in [SFA GitHub repository](https://github.com/patrickzib/SFA). This class read benchmarks and applications matrix under `/data`. After computing the model it write the applications segments prediction results on `/temps`.  

To compile and run the file individually, you should execute the following commands:  

```console
foo@bar:~/trident-poseidon/poseidon-scripts/SFA$ javac -cp lib/\\* -d classes @sources.txt
foo@bar:~/trident-poseidon/poseidon-scripts/SFA$ javac -cp lib/\\* -d ./classes sfa.MTSClassification
```

**The code for this step is under ```poseidon-scripts/SFA```. This directory is a clone from Symbolic Frourier Approximation (SFA) [GitHub repository](https://github.com/patrickzib/SFA) with only the needed files for WEASEL + MUSE execution. Few changes has been made to the original file. Original Author: [patrickzib](https://github.com/patrickzib).**

#### 5. Knowledge Generation
Finally, the results are analyzed and reported with `knowledge_mapper.py`. This script read the applications information, the benchmark information and the prediction results under `/temps`, and if available, the knowledge database under `/data`. Finally, it do a mapping between all of these components to produce knowledge. In addition to this, using the help of `poseidon_plotter.py` script, it can plot the results on a canvas.  


To run the entire pipeline of Poseidon, use `poseidon.py` script, which calls in subprocess each of the steps explained previously.  


## Metrics Glossary ##

### General ###

- **S0/S1/S2/.../SN**:		    Socket (processor)
- **C0/C1/C2/C3**:            Core
- **P1/P2/P3/.../PN**:		    Ports
- **UOPS**:                       uOp, or micro-op, is a low-level hardware operation. The CPU Front-End is responsible for fetching the program code represented in architectural instructions and decoding them into one or more uOps.

### Raw Metrics File: ###

- **READ BW (MIB)**:			    Quantity of Read Bandwidth in MIB
- **WRITE BW (MIB)**:			    Quantity of Written Bandwith in MIB
- **ACT COUNT**:			        DRAM Activate Count (# of times the DRAM became active)
- **PAGE ACT COUNT**:			    DRAM Page Activate Count (# of times the Page DRAM became active)
- **PRE_COUNT.PAGE_MISS**:	    DRAM Precharg events due to page Miss (i.e. page conflict)
- **INST**:                        Total number of instructions retired
- **CYC**:                         Total number of cycles
- **IDQ UPS NOT DELV CORE**:       This event counts the number of uops not delivered to Resource Allocation Table (RAT) per thread adding “4 – x” when Resource Allocation Table (RAT) is not stalled and Instruction Decode Queue (IDQ) delivers x uops to Resource Allocation Table (RAT) (where x belongs to {0,1,2,3}). Counting does not cover cases when: a. IDQ-Resource Allocation Table (RAT) pipe serves the other thread; b. Resource Allocation Table (RAT) is stalled for the thread (including uop drops and clear BE conditions); c. Instruction Decode Queue (IDQ) delivers four uops.
- **UOPS ISSUED**:                 This event counts the number of Uops issued by the Resource Allocation Table (RAT) to the reservation station (RS).
- **UOPS RETIRED**:                This event counts all actually retired uops. Counting increments by two for micro-fused uops, and by one for macro-fused and other uops. Maximal increment value for one cycle is eight.
- **INT MISC RECOVERY CYCLES**:    Core cycles the allocator was stalled due to recovery from earlier clear event for any thread running on the physical core (e.g. misprediction or memory nuke).
- **S[N] UOPS EXEC P[M]**:			Cycles of core N when uops are dispatched to port M.

### Processed Metrics File ###

- **slots**:           4 * cpu_clk_unhalted
- **fe bound**:        idq_uops_not_delivered / slots
- **bad spec**:        (uops_issued - uops_retired_slots + 4*recovery_cycles) / slots
- **retiring**:        uops_retired_slots / slots
- **be bound**:        1 - fe_bound - bad_spec - retiring
- **RBW**:             Total read bandwidth
- **WBW**:             Total written bandwidth
- **RM BW**:           Intermediate variable (not useful)
- **TOT BW**:          Intermediate variable (not useful)
- **CY**:              Cycles
- **IN**:              Instructions
- **IPC**:             Instructions per cycle (IN / CY)
- **RATIO**:           Ratio of the port usage
- **PO**:
- **PM**:
- **PCT_REQUESTS_PAGE_EMPTY** = (ACT_COUNT - PRE_COUNT.PAGE_MISS) / (CAS_COUNT.RD + CAS_COUNT.WR)
- **PCT_REQUESTS_PAGE_MISS (i.e., conflict)** = PCT_COUNT.PAGE_MISS / (CASE_COUNT.RD + CAS_COUNT.WR)
- **PCT_REQUESTS_PAGE_HIT** = 1 - PCT_REQUESTS_PAGE_EMPTY - PCT_REQUESTS_PAGE_MISS
- **MEM_BW_READS** = 64 * CAS_COUNT.RD
- **MEM_BW_WRITES** = 64 * CAS_COUNT.WR

## References ##

[1]	 Willhalm, T., Dementiev, R., & Fay, P. (2012). Intel performance counter monitor-a better way to measure cpu utilization. Dosegljivo: https://software.intel.com/en-us/articles/intel-performance-counter-monitor-a-better-way-to-measure-cpu-utilization.   [Dostopano: September 2014].
[2] 	Delgado, N., Gates, A. Q., & Roach, S. (2004). A taxonomy and catalog of runtime software-fault monitoring tools. IEEE Transactions on software Engineering, 30(12), 859-872.  
[3] 	Schäfer, P., & Leser, U. (2017, November). Fast and accurate time series classification with weasel. In Proceedings of the 2017 ACM on Conference on Information and Knowledge Management (pp. 637-646). ACM.  
[4] 	Schäfer, P., & Leser, U. (2017). Multivariate time series classification with weasel+ muse. arXiv preprint arXiv:1711.11343.  
[5] 	Keogh, E., & Lin, J. (2005). Clustering of time-series subsequences is meaningless: implications for previous and future research. Knowledge and information systems, 8(2), 154-177. [6] Making subsequence time series clustering meaningful  
[7] 	Fryzlewicz, P. (2014). Wild binary segmentation for multiple change-point detection. The Annals of Statistics, 42(6), 2243-2281.  
[8] 	Lin, J., Khade, R., & Li, Y. (2012). Rotation-invariant similarity in time series using bag-of-patterns representation. Journal of Intelligent Information Systems, 39(2), 287-315.  
[9] 	Karim, F., Majumdar, S., Darabi, H., & Harford, S. (2018). Multivariate LSTM-FCNs for Time Series Classification. arXiv preprint arXiv:1801.04503.  
[10] 	Salvador, S., & Chan, P. (2007). Toward accurate dynamic time warping in linear time and space. Intelligent Data Analysis, 11(5), 561-580.  
[11] 	Symbolic Fourier Approximation, WEASEL & WEASEL + MUSE. Github Repository: https://github.com/patrickzib/SFA  
[12] 	Truong, C., Oudre, L., & Vayatis, N. (2018). A review of change point detection methods. arXiv preprint arXiv:1801.00718. GitHub repository: https://github.com/deepcharles/ruptures  
[13] 	Schäfer, P., & Högqvist, M. (2012, March). SFA: a symbolic fourier approximation and index for similarity search in high dimensional datasets. In Proceedings of the 15th International Conference on Extending Database Technology (pp. 516-527). ACM.  
